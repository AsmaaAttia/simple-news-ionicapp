import { Component, OnInit } from '@angular/core';
import { NewsService } from '../services/news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  data:any;
  constructor( private newsService:NewsService ,private router:Router) { }

  ngOnInit() {
    this.newsService.getDate('everything?q=bitcoin&from=2019-05-18&sortBy=publishedAt').subscribe(data=>
      {
        console.log(data);
         this.data=data;
      });
  }
  onGoingToSinglePage(article){
    this.newsService.currentArticle=article;
    this.router.navigate(['/news-single']);
  }

}
