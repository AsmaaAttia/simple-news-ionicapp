import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

const Api_Url=environment.Api_Url;
const Api_Key=environment.Api_Key;


@Injectable({
  providedIn: 'root'
})
export class NewsService {
  currentArticle:any;
  constructor( private http:HttpClient) { }

  getDate(url){
    return this.http.get(`${Api_Url}/${url}&apiKey=${Api_Key}`);
  }
}
